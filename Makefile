apk = platforms/android/build/outputs/apk/android-debug.apk
style_src = $(shell find src/style/ -name '*.sass')
coffeescript_src = $(shell find src/ -name '*.coffee')
images_src = $(shell find src/img/ -name '*.svg')
images = $(patsubst src/%.svg, www/%.png, $(images_src))

all: $(apk)

adb-install: all
	adb install -f $(apk)

.PHONY: web

web: www/index.html www/index.css www/index.js $(images)

$(apk): .android.stamp config.xml web
	cordova build
	touch $@

.android.stamp:
	mkdir -p www # cordova will refuse to add the platform without www/
	cordova platform add android
	touch $@

www/index.html: src/index.html
	mkdir -p www/
	cp $< $@

www/index.css: $(style_src)
	mkdir -p www/
	sprockets \
    --include=src/style src/style/index.sass > $@ \
	|| ($(RM) $@; false)

www/index.js: $(coffeescript_src)
	mkdir -p www/
	sprockets --include=src src/index.coffee > $@ \
	|| ($(RM) $@; false)

$(images): www/img/%.png : src/img/%.svg
	mkdir -p www/img/
	inkscape --export-png=$@ $^

clean:
	$(RM) -r www/ platforms/ plugins/ .android.stamp
